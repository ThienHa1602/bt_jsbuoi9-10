function showMessage(idTag, message) {
  document.getElementById(idTag).innerHTML = message;
}
// KT độ dài
function kiemTraDoDai(value, idErr, min, max) {
  var length = value.length;
  if (length >= min && length <= max) {
    showMessage(idErr, "");
    return true;
  } else {
    showMessage(idErr, `Trường này phải gồm ${min} đến ${max} kí tự`);
    return false;
  }
}
// KT chuỗi
function kiemTraChuoi(value, idErr) {
  var re = /^[a-zA-Z\-]+$/;
  if (re.test(value)) {
    showMessage(idErr, "");
    return true;
  } else {
    showMessage(idErr, `Trường này chỉ gồm chữ`);
    return false;
  }
}
// KT email
function kiemTraEmail(email) {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  var isEmail = re.test(email);
  if (isEmail) {
    showMessage("tbEmail", "");
    return true;
  } else {
    showMessage("tbEmail", "Email không hợp lệ");
    return false;
  }
}
// KT matKhau
function kiemTraMatKhau(matKhau, idErr) {
  var isMatKhau =
    /[A-Z]/.test(matKhau) &&
    /[a-z]/.test(matKhau) &&
    /[0-9]/.test(matKhau) &&
    /[^A-Za-z0-9]/.test(matKhau) &&
    matKhau.length >= 6 &&
    matKhau.length <= 10;
  if (isMatKhau) {
    showMessage(idErr, "");
    return true;
  } else {
    showMessage(
      idErr,
      `Mật Khẩu phải từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)`
    );
    return false;
  }
}
// KT lương
function kiemTraLuong(value, idErr) {
  if (value >= 1000000 && value <= 20000000) {
    showMessage(idErr, "");
    return true;
  } else {
    showMessage(idErr, `Lương không đúng`);
    return false;
  }
}
// KT giờ làm
function kiemTraGioLam(value, idErr) {
  if (value >= 80 && value <= 200) {
    showMessage(idErr, "");
    return true;
  } else {
    showMessage(idErr, `Giờ làm không đúng`);
    return false;
  }
}
// KT trùng
function kiemTraTrung(tk, dsnv, idErr) {
  var viTri = dsnv.findIndex(function (item) {
    return item.tk == tk;
  });
  if (viTri != -1) {
    showMessage(idErr, `Tài khoản đã tồn tại`);
    return false;
  } else {
    showMessage(idErr, "");
    return true;
  }
}
