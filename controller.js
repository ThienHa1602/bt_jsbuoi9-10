function layThongTinTuForm() {
  var tk = document.getElementById("tknv").value;
  var ten = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var matKhau = document.getElementById("password").value;
  var ngayLam = document.getElementById("datepicker").value;
  var luong = document.getElementById("luongCB").value * 1;
  var chucVu = document.getElementById("chucvu").value;
  var gioLam = document.getElementById("gioLam").value * 1;
  return new NhanVien(tk, ten, email, matKhau, ngayLam, luong, chucVu, gioLam);
}
function renderDSNV(dsnv) {
  var contentHTML = "";
  for (var i = 0; i < dsnv.length; i++) {
    console.log("dsnv.length: ", dsnv.length);
    var item = dsnv[i];
    var contentTr = `
    <tr>
    <td> ${item.tk} </td>
    <td> ${item.ten} </td>
    <td> ${item.email} </td>
    <td> ${item.ngayLam} </td>
    <td> ${item.chucVu} </td>
    <td> ${item.tongLuong()} </td>
    <td> ${item.xepLoai()} </td>
    <button onclick="xoaNV('${item.tk}')" class="btn btn-danger">
    Xóa
    </button>
    <button onclick="suaNV('${item.tk}')" class="btn btn-danger">
    Sửa
    </button>
</tr>
    `;
    contentHTML += contentTr;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}
