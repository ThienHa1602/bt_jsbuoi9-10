dsnv = [];

function themNV() {
  var nv = layThongTinTuForm();
  // validate
  isValid =
    kiemTraDoDai(nv.tk, "tbTKNV", 4, 6) && kiemTraTrung(nv.tk, dsnv, "tbTKNV");
  isValid = isValid & kiemTraChuoi(nv.ten, "tbTen");
  isValid = isValid & kiemTraEmail(nv.email);
  isValid = isValid & kiemTraMatKhau(nv.matKhau, "tbMatKhau");
  isValid = isValid & kiemTraLuong(nv.luong, "tbLuongCB");
  isValid = isValid & kiemTraGioLam(nv.gioLam, "tbGiolam");
  if (isValid) {
    // push phần tử mới vào dssv
    dsnv.push(nv);
    renderDSNV(dsnv);
    resetForm();
  }
}
function xoaNV(tk) {
  var viTri = -1;
  for (var i = 0; i < dsnv.length; i++) {
    if (dsnv[i].tk == tk) {
      viTri = i;
    }
  }
  if (viTri != -1) {
    dsnv.splice(viTri, 1);
    renderDSNV(dsnv);
  }
}
function suaNV(tk) {
  var viTri = dsnv.findIndex(function (item) {
    return item.tk == tk;
  });
  var nv = dsnv[viTri];
  document.getElementById("tknv").value = nv.tk;
  document.getElementById("name").value = nv.ten;
  document.getElementById("email").value = nv.email;
  document.getElementById("password").value = nv.matKhau;
  document.getElementById("datepicker").value = nv.ngayLam;
  document.getElementById("luongCB").value = nv.luong;
  document.getElementById("chucvu").value = nv.chucVu;
  document.getElementById("gioLam").value = nv.gioLam;
}
function capNhatNV() {
  var nv = layThongTinTuForm();
  var viTri = dsnv.findIndex(function (item) {
    return item.tk == nv.tk;
  });
  // validate
  isValid = kiemTraDoDai(nv.tk, "tbTKNV", 4, 6);
  isValid = isValid & kiemTraChuoi(nv.ten, "tbTen");
  isValid = isValid & kiemTraEmail(nv.email);
  isValid = isValid & kiemTraMatKhau(nv.matKhau, "tbMatKhau");
  isValid = isValid & kiemTraLuong(nv.luong, "tbLuongCB");
  isValid = isValid & kiemTraGioLam(nv.gioLam, "tbGiolam");
  if (isValid) {
    dsnv[viTri] = nv;
    renderDSNV(dsnv);
    resetForm();
  }
}
function resetForm() {
  document.getElementById("formQLNV").reset();
}
function timNV() {
  var loaiNV = document.getElementById("searchName").value;
  var nv = [];
  for (var i = 0; i < dsnv.length; i++) {
    if (dsnv[i].xepLoai() == loaiNV) {
      nv.push(dsnv[i]);
    }
    renderDSNV(nv);
  }
}
