function NhanVien(tk, ten, email, matKhau, ngayLam, luong, chucVu, gioLam) {
  this.tk = tk;
  this.ten = ten;
  this.email = email;
  this.matKhau = matKhau;
  this.ngayLam = ngayLam;
  this.luong = luong;
  this.chucVu = chucVu;
  this.gioLam = gioLam;
  this.tongLuong = function () {
    if (this.chucVu == "Sếp") {
      return this.luong * 3;
    } else if (this.chucVu == "Trưởng phòng") {
      return this.luong * 2;
    } else if (this.chucVu == "Nhân viên") {
      return this.luong;
    } else {
    }
  };
  this.xepLoai = function () {
    if (this.chucVu == "Nhân viên") {
      if (this.gioLam >= 192) {
        return "Nhân viên xuất sắc";
      } else if (this.gioLam >= 176) {
        return "Nhân viên giỏi";
      } else if (this.gioLam >= 160) {
        return "Nhân viên khá";
      } else {
        return "Nhân viên trung bình";
      }
    } else {
      return "";
    }
  };
}
